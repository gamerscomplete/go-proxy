package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
)

func main() {
	host := flag.String("host", "", "The URL of the PDF document to fetch")
	port := flag.Int("port", 7989, "The URL of the PDF document to fetch")
	sPort := flag.Int("sport", 80, "The URL of the PDF document to fetch")
	flag.Parse()

	if *host == "" {
		fmt.Println("Host flag required")
		return
	}

	// Replace "http://your-web-application-url.com" with the actual URL of your web application
	targetURL, err := url.Parse(*host + ":" + strconv.Itoa(*port))
	if err != nil {
		log.Fatal(err)
	}

	proxy := httputil.NewSingleHostReverseProxy(targetURL)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Set custom headers or perform any modifications on the request if needed
		r.Header.Set("X-Forwarded-For", r.RemoteAddr)

		// Serve the request through the proxy
		proxy.ServeHTTP(w, r)
	})

	// Replace ":8080" with the desired port to run the proxy on
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(*sPort), nil))
}
